/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cliente;

import Modelo.Fichero;
import Modelo.Usuario;
import Vista.Vista;
import Vista.VistaVideo;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import java.util.Base64;





/**
 *
 * @author Jose
 */

public class ControladorCliente {
    ArrayList <Fichero> ficheros = new ArrayList();
    ArrayList <Usuario> usuarios = new ArrayList();
    
    String hostName;
    int portNumber;
    Socket kkSocket;
    PrintWriter out;
    BufferedReader in;
    BufferedReader stdIn;
    Vista ventana;
    String login = null;
    int length = 0;    
    boolean exit = false;  
    boolean creado=false;
    VistaVideo video=null;
    
     public ControladorCliente(String hostName, int portNumber, Vista ventana) {
        this.hostName = hostName;
        this.portNumber = portNumber;
        this.ventana = ventana;
    }
    
    public ControladorCliente(){
        this.hostName = "localhost";
        this.portNumber = 4444;
    }
    
    public String getLogin(){
        return this.login;
    }  
    
    public void clientOn(String cadena) throws IOException {       
    
            try{
            kkSocket = new Socket(hostName, portNumber);
            out = new PrintWriter(kkSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream()));       
            stdIn = new BufferedReader(new InputStreamReader(System.in));
            String fromServer;
            String fromUser;
            
            if(cadena != null){
                System.out.println(cadena);
                out.println(cadena);
                this.exit = false;
            }           
                
            while ((fromServer = in.readLine()) !=null) {
                System.out.println("al principio del while"+fromServer);
                String[] strings = fromServer.split("#"); 
                String actual="";
                System.out.println("mensaje split"+strings[1]);
                if(strings[1].equals("RESPONSE_MULTIMEDIA_PHOTO")){
                    System.out.println("estoy dentro del if");
                   File file=null;
                   boolean terminado =false;
                   
                   if(strings[1].equals("RESPONSE_MULTIMEDIA_PHOTO")){
                        file = new File("C:\\Users\\angel\\Desktop\\recuPSP\\PracticaPSPCliente\\data\\"+strings[2]);
                        actual="foto";
                        System.out.println("creacion de la foto");
                   } 
                   else {
                        if (strings[1].equals("RESPONSE_MULTIMEDIA_VIDEO")) {
                            file = new File("C:\\Users\\angel\\Desktop\\recuPSP\\PracticaPSPCliente\\data\\"+strings[2]);
                            actual="video";
                            System.out.println("creacion del video");
                        }
                    }
                   long longitudFichero = Long.parseLong(strings[3]);
                   long totalPaquetes= longitudFichero/512;
                    FileOutputStream escritura = new FileOutputStream(file);
                    System.out.println("debajo del fileoutput");
                    int nPaquetes =0;
                    while (terminado==false) {
                        System.out.println("estoy en el segundo while terminado");
                        String mCodificado = strings[5];
                        System.out.println("bytes: "+strings[5]);
                        //byte[] bytesDecodificados = java.util.Base64.getDecoder().decode(mCodificado.getBytes());
                        byte[] bytesDecodificados = java.util.Base64.getDecoder().decode(mCodificado.getBytes());
                        System.out.println("descodificados: "+Arrays.toString(bytesDecodificados));
                        escritura.write(bytesDecodificados);
                        
                        if ((totalPaquetes - nPaquetes) <= 0) {
                            terminado=true;
                            System.out.println("terminado true");
                        }else {
                            
                            if (terminado==false) {
                                fromServer = in.readLine();
                                strings = fromServer.split("#");
                                ventana.meteralarea("Server: " + fromServer + "\n");
                                System.out.println("if terminado false");
                            }
                        }
                        nPaquetes++;
                    }
                    escritura.close();
            }
                System.out.println("DEBUGPAQUETE: " +fromServer);
                if (fromServer.contains("#END_PAQUETE")) {
                    if (actual.equals("foto")) {
                        ventana.mostrarFotoActual();
                        enviarMensaje("THANOS_PROTOCOL#"+login+"#PHOTO_RECEIVED#"+strings[3]);
                        System.out.println("gfjhgjd");
                    } else {
                        enviarMensaje("THANOS_PROTOCOL#"+login+"#VIDEO_RECEIVED#"+strings[3]);
                        
                        System.out.println("esto es un videooo");
                        
                        
                        if (creado==false) {
                            
                            
                            video = new VistaVideo();
                            video.iniciar();
                            creado=true;
                        } else {
                            video.start(video.getPrimarySt());
                        }
                        
                        
                    }
                }
                if (strings[1].equals("OK_USER_VALIDATED")){
                    this.exit = true;
                    System.out.println(fromServer);
                    int i = 6;
                    while (!strings[i].equals("END_TRANSMISION")) {                        
                        Fichero f = new Fichero(Integer.parseInt(strings[i]),strings[i+1],strings[i+2],strings[i+3],Integer.parseInt(strings[i+4]));
                        
                        ficheros.add(f);
                        i= i+5;
                    }
                    ventana.mostrarTablaFicheros(ficheros);      
                }
            }
            System.out.println("detras del while"+fromServer);
            
            
            } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
            } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +hostName);
            System.exit(1);
            }    
    
    }
    
    public void enviarMensaje (String cadena) {
        try {
            out = new PrintWriter(kkSocket.getOutputStream(), true);
        } catch (IOException ex) {
            Logger.getLogger(ControladorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("DEBUG: "+cadena);
        out.println(cadena);
        
    } 
}
