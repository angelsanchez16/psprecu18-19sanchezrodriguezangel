/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Angel
 */

public class Fichero {
    private int idfichero;
    private String ruta;  
    private String nombre;
    private String tipo;
    private int idusuario;

    public Fichero(int idfichero, String ruta, String nombre, String tipo,int idusuario) {
        this.idfichero = idfichero;
        this.ruta = ruta;
        this.nombre = nombre;
        this.tipo = tipo;
        this.idusuario=idusuario;
    }

    public Fichero(){
        this.idfichero = 0;
        this.ruta = null;
        this.nombre = null;
        this.tipo = null;
        this.idusuario=0;
    }

   

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getIdfichero() {
        return idfichero;
    }

    public void setIdfichero(int idfichero) {
        this.idfichero = idfichero;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
     public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }
}
