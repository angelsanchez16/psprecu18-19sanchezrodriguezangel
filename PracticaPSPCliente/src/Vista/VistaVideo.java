/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import static com.sun.javafx.scene.control.skin.Utils.getResource;
import java.io.File;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.stage.Stage;

/**
 *
 * @author admin
 */
public class VistaVideo extends Application {

    Stage primarySt;

    public Stage getPrimarySt() {
        return primarySt;
    }
    
    @Override
    public void start(Stage primaryStage) {
        
        primarySt=primaryStage;
        
        StackPane root = new StackPane();
        
        File file = new File(".\\Data\\video.mp4");
        
        MediaPlayer reproductor= new MediaPlayer(new Media(file.toURI().toString()));
        
        //MediaPlayer reproductor= new MediaPlayer(new Media(getClass().getResource("frijoles.mp4").toExternalForm()));
        
        MediaView mv = new MediaView(reproductor);
        
        root.getChildren().add(mv);
        
        Scene scene = new Scene(root, 1024, 768);
        
        
        primaryStage.setScene(scene);
        primaryStage.show();
        
        reproductor.setVolume(0.7);
       
        reproductor.play();
    }
    
    public void iniciar(){
        Application.launch();
    }
    
}
