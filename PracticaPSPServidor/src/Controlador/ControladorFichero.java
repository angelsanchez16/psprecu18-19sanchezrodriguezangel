/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Conexion;
import Modelo.Fichero;
import Modelo.ModeloFichero;
import Modelo.ModeloUsuario;
import Modelo.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class ControladorFichero {
    ModeloFichero mf = null;
    ArrayList<Fichero> listaFicheros = null;
    int idUsuario;

    public ControladorFichero() {
        this.mf = new ModeloFichero();
        this.listaFicheros =  new ArrayList<Fichero>();
    }     
    
    public  ArrayList <Fichero> getFichero(int idUsuario) throws SQLException, ClassNotFoundException {   
        this.idUsuario = idUsuario;
        this.listaFicheros = mf.getFichero(idUsuario);
        return this.listaFicheros;
        
    }
}
