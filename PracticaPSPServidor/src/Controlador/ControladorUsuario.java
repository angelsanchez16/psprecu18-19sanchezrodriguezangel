/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Conexion;
import Modelo.Fichero;
import Modelo.ModeloUsuario;
import Modelo.Usuario;
import Modelo.Validador;
import Vista.Vista;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class ControladorUsuario {   
    
     public void registrarUsuario(Usuario u){
        Conexion conn = new Conexion();	
        
	try {
            
                String sql = "INSERT INTO usuario (IDUSUARIO, NOMBRE, PASSWORD) VALUES (?,?,?)";
            try (PreparedStatement st = conn.getConnection().prepareStatement(sql)) {
                st.setInt(1, u.getIdusuario());               
                st.setString(2,u.getNombre());
                st.setString(3, u.getPassword());
                st.executeUpdate();
                //JOptionPane.showMessageDialog(null, "Se ha registrado exitosamente","Informacion",JOptionPane.INFORMATION_MESSAGE);
            }
		conn.desconectar();
			
	} catch (SQLException e) {
            System.out.println(e.getMessage());
		JOptionPane.showMessageDialog(null, "No se ha registrado");
	}
    }
    
    public Usuario buscarUsuario(String nombre){
        
	Conexion conn = new Conexion();
	Usuario u = new Usuario();
	boolean existe = false;
      
	try {
            Statement update = conn.getConnection().createStatement();        
            PreparedStatement consulta = conn.getConnection().prepareStatement("SELECT * FROM usuario where nombre = ?");
           
            consulta.setString(1, nombre);
            try (ResultSet rs = consulta.executeQuery()) {                
                while(rs.next()){                     
                    existe = true;
                    u.setIdusuario(rs.getInt("IDUSUARIO"));
                    u.setNombre(rs.getString("NOMBRE"));
                    u.setPassword(rs.getString("PASSWORD"));
                }
            }
            conn.desconectar();
			
					
	} catch (SQLException e) {
	JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
	}
	if (existe) {
            return u;
	}else 
            return null;				
    }   
    
}
