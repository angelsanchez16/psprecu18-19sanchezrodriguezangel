/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {
    static String basedatos = "psp";    
    static String login = "root";
    static String password = "";
    static String url = "jdbc:mysql://localhost:3306/psp" ;

    public static Connection conn = null;
    
    public Conexion() {
        try{
        
        Class.forName("com.mysql.jdbc.Driver");        
        conn = DriverManager.getConnection(url,login,password);

        if (conn!=null)
            System.out.println("Conexion a base de datos " + basedatos + " realizada con exito");
        }catch(SQLException e){
        System.out.println(e);
        }catch(ClassNotFoundException e){
        System.out.println(e);
        }catch(Exception e){
        System.out.println(e);
        }
    }
 
    public Connection getConnection(){
        return conn;
    }

    public void desconectar(){
        conn = null;
    }
    
     public static String getBasedatos() {
        return basedatos;
    }

    public static void setBasedatos(String basedatos) {
        Conexion.basedatos = basedatos;
    }

    public static String getLogin() {
        return login;
    }

    public static void setLogin(String login) {
        Conexion.login = login;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Conexion.password = password;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Conexion.url = url;
    }
}
