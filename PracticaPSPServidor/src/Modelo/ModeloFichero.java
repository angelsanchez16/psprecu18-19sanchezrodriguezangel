/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ModeloFichero {
    private static Conexion conexion;
    
    
    
     public ArrayList<Fichero> getFichero(int idUsuario) throws ClassNotFoundException, SQLException{         
         ArrayList <Fichero> ficheros = new ArrayList();
        conexion = new Conexion();
        java.sql.Connection conn = conexion.getConnection();
        Statement st;
        ResultSet rs;

        String query = "SELECT * "+ "FROM fichero WHERE idusuario ="+idUsuario;
         
        try{
            st = conn.createStatement();
            rs = st.executeQuery(query);
            while(rs.next()){
                int idf = rs.getInt("IDFICHERO");
                String nombre = rs.getString("NOMBRE");               
                String ruta = rs.getString("RUTA");
                String tipo = rs.getString("TIPO");
                int idu = rs.getInt("IDUSUARIO");
                
                Fichero p = new Fichero(idu, nombre, ruta, tipo,idf);
                ficheros.add(p);
            }
            conn.close();
            rs.close();
            st.close();
            System.out.println("Ficheros");
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        
        return ficheros;
    }
}