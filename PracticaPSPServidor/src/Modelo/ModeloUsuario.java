/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.ControladorUsuario;
import Vista.Vista;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ModeloUsuario {
    ControladorUsuario usuario = new ControladorUsuario();
     ArrayList <Usuario> usuarios = new ArrayList();  
    private static Conexion conexion;
    private static Vista ventana = new Vista(); 
    private static Validador validador = new Validador();
    
    public  ArrayList<Usuario> getUsuario() throws ClassNotFoundException, SQLException{         
        //ArrayList <Usuario> usuarios = new ArrayList();
        conexion = new Conexion();
        java.sql.Connection conn = conexion.getConnection();
        Statement st;
        ResultSet rs;

        String query = "SELECT * FROM usuario";
        
                       
        try{
            st = conn.createStatement();
            rs = st.executeQuery(query);
            while(rs.next()){
                int idusuario = rs.getInt("IDUSUARIO");
                String nombre = rs.getString("NOMBRE");               
                String password = rs.getString("PASSWORD");
                
                Usuario p = new Usuario(idusuario, nombre, password);
                usuarios.add(p);
            }
            conn.close();
            rs.close();
            st.close();           
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        
        return usuarios;
    }
    
    
      public Vista getVentana() {
	return ventana;
    }
    
    public void setVentana(Vista ventana) {
	this.ventana = ventana;
    }
    
    public Validador getValidador() {
	return validador;
    }
    
    public void setValidador(Validador validador) {
	this.validador = validador;
    }
    
    public void registrarUsuario(Usuario u) {
	validador.validarRegistro(u);
    }
	
    public Usuario buscarUsuario(String nombre) {
	return validador.validarBusqueda(nombre);
    }
	
}
