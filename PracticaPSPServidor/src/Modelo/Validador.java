/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.ControladorUsuario;
import javax.swing.JOptionPane;


public class Validador {
    private ModeloUsuario modelo;
    public static boolean validarbusqueda = false;
  
    
    public void validarRegistro(Usuario u) {
		ControladorUsuario uControlador;
		if (u.getNombre().length() == 3) {
			uControlador = new ControladorUsuario();
			uControlador.registrarUsuario(u);						
		}else
                    JOptionPane.showMessageDialog(null,"EL login debe ser de 9 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
			
    }
    
    public Usuario validarBusqueda(String nombre) {
		ControladorUsuario uControlador;
		
		try {
			uControlador = new ControladorUsuario();			
			return uControlador.buscarUsuario(nombre);
		
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			validarbusqueda = false;
		}
					
		return null;
    }
    
    
}
