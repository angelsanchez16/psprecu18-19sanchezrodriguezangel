package Servidor;



import Controlador.ControladorFichero;
import Controlador.ControladorUsuario;
import Modelo.ModeloUsuario;
import java.net.*;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HebraServidor extends Thread {
    private Socket socket = null;
    ModeloUsuario modelo = new ModeloUsuario();
    //ControladorFichero controlador1 = new ControladorFichero();
    boolean borrar = false;
  
    ArrayList<HebraServidor> usuarios = new ArrayList();
    ArrayList<HebraServidor> ficheros = new ArrayList();
    
    public PrintWriter broadcast;
    public Protocolo protocolo;

    public HebraServidor(Socket socket) {
        super("HebraServidor");
        this.socket = socket;
       // this.usuarios = usuarios;
    }   

    public Socket getSocket() {
        return socket;
    }   
    
    @Override
    public void run() {
        
        try (
            PrintWriter out  = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ) {
            
            broadcast = out;
            System.out.println("Server ON");             
            String inputLine = null, outputLine = null;            
            protocolo = new Protocolo();           
           
            while ((inputLine = in.readLine()) != null) { 
                
                        try {
                            System.out.println("RECIBO:"+inputLine);
                            outputLine = protocolo.processInput(inputLine,out,in,socket);
                            
                        } catch (ClassNotFoundException | SQLException ex) {
                            Logger.getLogger(HebraServidor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        if(!outputLine.equals("NOINPUT")){
                            out.println(outputLine);
                            System.out.println("ENVIO  "+outputLine);
                        }                
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  
}
