package Servidor;

import Controlador.ControladorFichero;
import Controlador.ControladorUsuario;
import Modelo.Fichero;
import Modelo.ModeloFichero;
import Modelo.ModeloUsuario;
import Modelo.Usuario;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Protocolo {
    ModeloUsuario m = new ModeloUsuario();
    ModeloFichero f = new ModeloFichero();  
    ArrayList<Fichero>listaficheros=new ArrayList<Fichero>();
    int idUsuario;
    String nombre;
    String infofichero;
    
    public String processInput(String theInput, PrintWriter out, BufferedReader in, Socket socket) throws ClassNotFoundException, SQLException, FileNotFoundException, IOException {
        String theOutput = "NOINPUT";           
         if(theInput.equals(null)) {
            theOutput = "Conectado";
        }           

        String[] strings = theInput.split("#");      
        Usuario u = new Usuario();                 
        u.setNombre(strings[2]);
        u.setPassword(strings[3]);

        System.out.println("RECIBIDO:"+theInput);


        if (strings[1].equals("LOGIN")) { 

            boolean logeado = false;
             u = m.buscarUsuario(strings[2]);


            if (u.getNombre().equals(strings[2]) && u.getPassword().equals(strings[3])) {

                logeado = true;



                nombre = u.getNombre();
                this.idUsuario = u.getIdusuario();
                listaficheros= f.getFichero(idUsuario);

                 for (int i=0;i<listaficheros.size();i++) {

                    int idu = listaficheros.get(i).getIdfichero();
                    String nombref = listaficheros.get(i).getRuta();
                    String  ruta= listaficheros.get(i).getNombre();
                    String tipo = listaficheros.get(i).getTipo();
                    int id = listaficheros.get(i).getIdu();


                    infofichero += "#"+id+"#"+ruta+"#"+nombref+"#"+tipo+"#"+idu;

                }
                  theOutput = "THANOS_PROTOCOL#OK_USER_VALIDATED"; 

                theOutput += "#" + u.getIdusuario()+ "#" + u.getNombre()+ "#" + u.getPassword()+"#"+infofichero+"#"+"END_TRANSMISION";


            }   

            else {                   
                theOutput = "THANOS_PROTOCOL#ERROR_INVALID_CREDENTIALS";
            }
        }

        if (strings[2].contains("GET_Fichero")){

            String idfichero = strings[3];
            File file = new File("C:\\Users\\angel\\Desktop\\recuPSP\\PracticaPSPServidor\\data\\"+strings[1]+"\\"+idfichero);

            long tamañoFichero = file.length();

            try {
                InputStream salida = new FileInputStream(file); 
                byte[] buffer = new byte[512];
                int i = 0;
                ByteArrayOutputStream output = new ByteArrayOutputStream();

                String cadenaProtocolo = "THANOS_PROTOCOL#RESPONSE_MULTIMEDIA_PHOTO#"+idfichero+"#"+tamañoFichero+"#"+512+"#";
                theOutput=cadenaProtocolo;
                while ((i = salida.read(buffer)) != -1) {
                    output.write(buffer);
                    System.out.println("MENSAJE PRECODIFICAR"+theOutput);
                    
                    theOutput = Base64.getEncoder().encodeToString(buffer);
                    System.out.println("MENSAJE CODIFICADO"+theOutput);
                    String decodificada = theOutput;
                    theOutput=theOutput;
                    byte[] descodificada = java.util.Base64.getDecoder().decode(decodificada);
                    System.out.println("MENSAJE DESCODIFICADO"+decodificada);
                }
                theOutput+=" #END_PAQUETE";
                //salida.close();

            } catch (FileNotFoundException ex) {
                  Logger.getLogger(Protocolo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        System.out.println("THEOUTPUT    "+theOutput);
        return theOutput;  
    }

}