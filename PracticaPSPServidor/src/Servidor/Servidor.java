package Servidor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paco
 */

public class Servidor {
    int portNumber = 4444;
    boolean listening = true;
    
    ArrayList <HebraServidor> usuarios = new ArrayList(); 
    ArrayList <HebraServidor> ficheros = new ArrayList(); 
    
    public Servidor(int port){
        this.portNumber = port;
        this.listening = true;
    }    

    public Servidor() {
        this.portNumber = 4444;
        this.listening = true;
    }
    
    public synchronized void serverOn() throws IOException {   
        
        System.out.println("Servidor conectado");
       
        try (ServerSocket serverSocket = new ServerSocket(portNumber)) { 
            while (listening) {
                    this.usuarios.add(new HebraServidor(serverSocket.accept()));
	            this.usuarios.get(this.usuarios.size()-1).start();
	        }
	    } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
    } 
    
}

